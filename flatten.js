function flatten(elements) {
    let outputArray = [];
    for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements[index])) {
            outputArray = outputArray.concat(flatten(elements[index]));
        } else {
            outputArray.push(elements[index]);
        }
    }
    return outputArray;
}


module.exports = flatten;