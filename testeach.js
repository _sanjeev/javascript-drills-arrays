const each = require('./each.js');
const index = 0;
const items = [1, 2, 3, 4, 5, 5];

let actual = [];

const cb = (value) => {
    actual.push(value);
}

each(items, cb);
console.log(actual);

const expected = [1, 2, 3, 4, 5, 5];

let equal = true;

for (let index = 0; index < actual.length; index++) {
    if (actual[index] !== expected[index]) {
        equal = false;
        console.log ('Different');
        break;
    }
}

if (equal) {
    console.log ('Same');
}