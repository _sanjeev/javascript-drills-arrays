function reduce (elements, cb, startingvalue) {
    let output = null;
    for (let i = 0; i < elements.length; i++) {
        startingvalue = cb (elements[i], startingvalue);
    }
    output = startingvalue;
    return output;
}

module.exports = reduce;