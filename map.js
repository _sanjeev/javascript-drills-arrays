function map(elements, cb) {
    let outputArray = [];
    for (let i = 0; i < elements.length; i++) {
        outputArray.push(cb(elements[i]));
    }
    return outputArray;
}

module.exports = map;

