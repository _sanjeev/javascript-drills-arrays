function filter (elements, cb) {
    let output = [];
    for (let index = 0; index < elements.length; index++) {
        if (cb (elements[index])) {
            output.push (elements[index]);
        }
    }
    return output;
}

module.exports = filter