const reduce = require ('./reduce');

const items = [1, 2, 3, 4, 5, 5];

const cb = (val, accumulator) => {
    return val + accumulator;
}

let actual = reduce (items, cb, 0);
console.log (actual);

const expected = 20;
if (actual === expected) {
    console.log ('Same');
} else {
    console.log ('Different');
}