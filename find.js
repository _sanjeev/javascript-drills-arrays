function find (elements, cb) {
    let result;
    for (let index = 0; index < elements.length; index++) {
        if (cb (elements[index], 5)) {
            result = elements[index];
            break;
        }
    }
    return result;
}

module.exports = find;