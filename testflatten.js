const flatten = require('./flatten.js');

const nestedArray = [1, [2], [[3]], [[[4]]]];

let actual = flatten(nestedArray);
console.log(actual);

let expected = [1, 2, 3, 4];

let equal = true;

for (let index = 0; index < actual.length; index++) {
    if (actual[index] !== expected[index]) {
        equal = false;
        console.log ('Different');
        break;
    }
}

if (equal) {
    console.log ('Same');
}