const filter = require('./filter');

const items = [1, 2, 3, 4, 5, 5];

function cb(element) {
    if (element % 2 === 0) {
        return true;
    }
}

let actual = filter(items, cb);
console.log(actual);

const expected = [2, 4];
let equal = true;

for (let index = 0; index < actual.length; index++) {
    if (actual[index] !== expected[index]) {
        equal = false;
        console.log ('Different');
        break;
    }
}

if (equal) {
    console.log ('Same');
}